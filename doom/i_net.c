
#include <stdlib.h>

#include "i_net.h"
#include "doomstat.h"
#include "m_argv.h"

void I_InitNetwork(void) {
    
    doomcom = calloc(1, sizeof(*doomcom));
    
    int i = M_CheckParm("-dup");
    if (i && i< myargc-1) {
        doomcom->ticdup = myargv[i+1][0]-'0';
        if (doomcom->ticdup < 1) {
            doomcom->ticdup = 1;
        }
        if (doomcom->ticdup > 9) {
            doomcom->ticdup = 9;
        }
    } else {
        doomcom-> ticdup = 1;
    }
        
    if (M_CheckParm("-extratic")) {
        doomcom->extratics = 1;
    } else {
        doomcom->extratics = 0;
    }
                
    netgame = false;
    doomcom->id = DOOMCOM_ID;
    doomcom->numplayers = doomcom->numnodes = 1;
    doomcom->deathmatch = false;
    doomcom->consoleplayer = 0;
}

void I_NetCmd(void) {
}

