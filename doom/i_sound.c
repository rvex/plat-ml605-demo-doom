
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#ifdef __RVEX__
#include <soundcard.h>
#else
#include <linux/soundcard.h>
#endif

#include "i_sound.h"

int samplerate = 11025;

static void music_synthesize(short *buffer, int count);

static int aud_fd = -1;

void I_InitSound() {
    
    // Open audio file.
    aud_fd = open("/dev/dsp", O_WRONLY);
    if (aud_fd < 0) {
        perror("open of /dev/dsp failed");
        exit(1);
    }

    // Configure sound device.
    int val = AFMT_S16_NE;
    if (ioctl(aud_fd, SNDCTL_DSP_SETFMT, &val) < 0) perror("SOUND_PCM_WRITE_BITS");
    val = 1;
    if (ioctl(aud_fd, SNDCTL_DSP_STEREO, &val) < 0) perror("SNDCTL_DSP_STEREO");
    if (ioctl(aud_fd, SNDCTL_DSP_SPEED, &samplerate) < 0) perror("SNDCTL_DSP_SPEED");
    
    if (samplerate > 48000) {
        fprintf(stderr, "Minimum samplerate supported by /dev/dsp (%d) is too high, need 48000 max\n", samplerate);
        exit(1);
    }
    printf("Samplerate: %d\n", samplerate);
    
    // Initialize music.
    I_InitMusic();
}

void I_UpdateSound(void) {
}

void I_SubmitSound(void) {
    
    // Try to get 125ms of audio in the buffer.
    int target = samplerate >> 3;
    
    // 125ms buffer at maximum supported samplerate.
    static short buf[(48000 >> 3) * 2];
    
    // Figure out how many samples remain in the buffer.
    // TODO, assuming 0, which will make this block eventually (unless we can't
    // keep up).
    int count = target;
    
    // Write the audio.
    music_synthesize(buf, count);
    int wrote = write(aud_fd, buf, count * 4);
    if (wrote < 0) {
        perror("audio write");
    } else {
        printf("wrote %d bytes to audio out\n", wrote);
    }
    
}

void I_ShutdownSound(void) {
}

void I_SetChannels(void) {
}

int I_GetSfxLumpNum(sfxinfo_t* sfx) {
    char namebuf[9];
    sprintf(namebuf, "ds%s", sfx->name);
    return W_GetNumForName(namebuf);
}

int I_StartSound(int id, int vol, int sep, int pitch, int priority) {
    return 0;
}

void I_StopSound(int handle) {
}

int I_SoundIsPlaying(int handle) {
    return 0;
}

void I_UpdateSoundParams(int handle, int vol, int sep, int pitch) {
}



#define DISABLE_MUSIC

#ifndef DISABLE_MUSIC


#define TSF_IMPLEMENTATION
#include "i_sound_tsf.h"

static int mus_vol = 0;

#ifdef __BIG_ENDIAN__
#define MUS_SWAP(x) (((x) << 8) | ((x) >> 8))
#else
#define MUS_SWAP(x) (x)
#endif

typedef struct mus_hdr_t {
    unsigned char ID[4];
    unsigned short scoreLen;
    unsigned short scoreStart;
    unsigned short channels;
    unsigned short sec_channels;
    unsigned short instrCnt;
    unsigned short dummy;
    unsigned short instruments[];
} mus_hdr_t;

typedef struct mus_info_t {
    const mus_hdr_t *hdr;
    const unsigned char *data;
    int data_len;
} mus_info_t;

typedef struct mus_playback_state_t {
    
    // Currently playing track.
    const mus_info_t *track;
    
    // Previous velocity for each channel.
    unsigned char vel[16];
    
    // Current position in data.
    const unsigned char *data;
    
    // Number of audio tics (1/140s) to wait before
    int tic_wait;
    
    // Whether we need to loop the current song.
    char looping;
    
    // Whether we're currently playing.
    char playing;
    
    // Whether the synthesizer is currently enabled.
    char enabled;
    
} mus_playback_state_t;

/**
 * Song data.
 */
#define MAX_SONGS 16
static mus_info_t songs[16];

/**
 * Playback state.
 */
static mus_playback_state_t mus_stat;

/**
 * TSF state.
 */
static tsf *mus_tsf;


void I_InitMusic(void) {
    fprintf(stderr, "Loading soundfont...\n");
#ifdef __RVEX__
    mus_tsf = tsf_load_filename("/part2/doom/soundfont.sf2");
#else
    mus_tsf = tsf_load_filename("soundfont.sf2");
#endif
    if (!mus_tsf) {
        fprintf(stderr, "Failed to load soundfont! Music won't work.\n");
    } else {
        fprintf(stderr, "Soundfont loaded.\n");
        tsf_set_output(mus_tsf, TSF_STEREO_INTERLEAVED, samplerate, 0);
    }
}

void I_ShutdownMusic(void) {
}

void I_SetMusicVolume(int volume) {
    mus_vol = volume;
}

int I_RegisterSong(void *data) {
    
    // Find a free handle.
    int handle;
    for (handle = 0; handle < MAX_SONGS; handle++) {
        if (!songs[handle].hdr) {
            break;
        }
    }
    if (handle == MAX_SONGS) {
        return -1;
    }
    
    // Save the requisite info.
    const mus_hdr_t *hdr = (const mus_hdr_t*)data;
    songs[handle].hdr = hdr;
    songs[handle].data = (const unsigned char*)data + MUS_SWAP(hdr->scoreStart);
    songs[handle].data_len = MUS_SWAP(hdr->scoreLen);
    
    printf("Registering song %d:\n", handle);
    printf("  len = %d\n",              MUS_SWAP(hdr->scoreLen));
    printf("  channels = %d\n",         MUS_SWAP(hdr->channels));
    printf("  sec_channels = %d\n",     MUS_SWAP(hdr->sec_channels));
    printf("  instrCnt = %d\n",         MUS_SWAP(hdr->instrCnt));
    
    // Return the handle.
    return handle;
}

void I_UnRegisterSong(int handle) {
    songs[handle].hdr = NULL;
}

void I_PlaySong(int handle, int looping) {
    if (!mus_tsf) return;
    
    // Set the current track.
    mus_stat.track = songs + handle;
    
    // Load the channel -> instrument mapping from the mus file.
    const mus_hdr_t *hdr = songs[handle].hdr;
    int channel;
    for (channel = 0; channel < MUS_SWAP(hdr->instrCnt) && channel < 15; channel++) {
        tsf_channel_set_preset(mus_tsf, channel, MUS_SWAP(hdr->instruments[channel]));
        tsf_channel_set_pan(mus_tsf, channel, 0.0f);
        tsf_channel_set_volume(mus_tsf, channel, 1.0f);
        mus_stat.vel[channel] = 127;
    }
    
    // Channel 15 is always percussion.
    tsf_channel_set_preset(mus_tsf, 15, 253);
    tsf_channel_set_pan(mus_tsf, 15, 0.0f);
    tsf_channel_set_volume(mus_tsf, 15, 1.0f);
    mus_stat.vel[15] = 127;
    
    // Initialize state.
    mus_stat.data = songs[handle].data;
    mus_stat.tic_wait = 0;
    mus_stat.looping = looping;
    mus_stat.playing = 1;
    mus_stat.enabled = 1;
}

void I_StopSong(int handle) {
    if (!mus_tsf) return;
    mus_stat.playing = 0;
    mus_stat.enabled = 0;
    int channel;
    for (channel = 0; channel < 15; channel++) {
        tsf_channel_sounds_off_all(mus_tsf, channel);
        tsf_channel_set_pan(mus_tsf, channel, 0.0f);
        tsf_channel_set_volume(mus_tsf, channel, 1.0f);
        tsf_channel_set_pitchwheel(mus_tsf, channel, 8192);
    }
}

void I_PauseSong(int handle) {
    if (!mus_tsf) return;
    mus_stat.enabled = 0;
}

void I_ResumeSong(int handle) {
    if (!mus_tsf) return;
    mus_stat.enabled = 1;
}

/**
 * Synthesizes some music, or memsets to 0 if the music is disabled.
 */
static void music_synthesize(short *buffer, int count) {
    if (!mus_tsf || !mus_stat.enabled) {
        memset(buffer, 0, count * sizeof(short) * 2);
        return;
    }
    
    // Handle events from the song.
    if (mus_stat.playing) {
        if (mus_stat.tic_wait) {
            mus_stat.tic_wait--;
        } else {
            
            while (1) {
                
                unsigned char cmd = *mus_stat.data++;
                int last    = cmd & 0x80;
                int event   = cmd & 0x70;
                int channel = cmd & 0x0F;
                
                switch (event) {
                    
                    case 0x00: { // Note off.
                        unsigned char par = *mus_stat.data++;
                        int note = par & 0x7F;
                        tsf_channel_note_off(mus_tsf, channel, note);
                        break;
                    }
                    
                    case 0x10: { // Note on.
                        unsigned char par = *mus_stat.data++;
                        int note = par & 0x7F;
                        unsigned char vel;
                        if (par & 0x80) {
                            unsigned char par = *mus_stat.data++;
                            vel = par & 0x7F;
                            mus_stat.vel[channel] = vel;
                        } else {
                            vel = mus_stat.vel[channel];
                        }
                        tsf_channel_note_on(mus_tsf, channel, note, (float)vel * (1.0f / 127.0f));
                        break;
                    }
                    
                    case 0x20: { // Pitch wheel.
                        int pitch = *mus_stat.data++;
                        tsf_channel_set_pitchwheel(mus_tsf, channel, pitch << 6);
                        break;
                    }
                    
                    case 0x30: { // System event.
                        unsigned char event = *mus_stat.data++;
                        switch (event) {
                            case 10: // Sounds off.
                                tsf_channel_sounds_off_all(mus_tsf, channel);
                                break;
                            case 11: // Notes off.
                                tsf_channel_note_off_all(mus_tsf, channel);
                                break;
                            case 12: // Mono.
                                // Not supported by TSF.
                                break;
                            case 13: // Poly.
                                // Not supported by TSF.
                                break;
                            case 14: // Reset all controllers.
                                tsf_channel_set_pan(mus_tsf, channel, 0.0f);
                                tsf_channel_set_volume(mus_tsf, channel, 1.0f);
                                tsf_channel_set_pitchwheel(mus_tsf, channel, 8192);
                                break;
                        }
                        break;
                    }
                    
                    case 0x40: {// Change controller.
                        unsigned char controller = *mus_stat.data++;
                        unsigned char value = *mus_stat.data++;
                        switch (controller) {
                            case 3: // Volume: 0-silent, ~100-normal, 127-loud
                                tsf_channel_set_volume(mus_tsf, channel, value * (1.0f / 255.0f));
                                break;
                            case 4: // Pan (balance) pot: 0-left, 64-center (default), 127-right
                                tsf_channel_set_pan(mus_tsf, channel, (value - 64) * (1.0f / 63.0f));
                                break;
                            // There are more controllers, but TSF doesn't support them.
                        }
                        break;
                    }
                    
                    case 0x60: // End of song.
                        if (mus_stat.looping) {
                            mus_stat.data = mus_stat.track->data;
                        } else {
                            I_StopSong(0);
                        }
                        break;
                    
                    default:
                        fprintf(stderr, "mus decoding error!\n");
                        I_StopSong(0);
                        return;
                    
                }
                
                // Break out of loop when end of song is reached.
                if (event == 0x60) break;
                
                // Break out of loop when last is set.
                if (last) {
                    
                    // Read time information.
                    int time = 0;
                    while (1) {
                        unsigned char val = *mus_stat.data++;
                        time <<= 7;
                        time |= val & 0x7F;
                        if (!(val & 0x80)) break;
                    }
                    
                    if (time > 0) {
                        mus_stat.tic_wait = time - 1;
                        break;
                    }
                }
                
            }
            
        }
    }
    
    // Render.
    tsf_render_short(mus_tsf, buffer, count, 0);
}


#else /* DISABLE_MUSIC */



void I_InitMusic(void) {
}

void I_ShutdownMusic(void) {
}

void I_SetMusicVolume(int volume) {
}

int I_RegisterSong(void *data) {
    return 1;
}

void I_UnRegisterSong(int handle) {
}

void I_PlaySong(int handle, int looping) {
}

void I_StopSong(int handle) {
}

void I_PauseSong(int handle) {
}

void I_ResumeSong(int handle) {
}

int I_QrySongPlaying(int handle) {
    return 0;
}

static void music_synthesize(short *buffer, int count) {
    memset(buffer, 0, count * sizeof(short) * 2);
}


#endif
