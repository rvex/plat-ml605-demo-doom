
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <stdarg.h>

#ifdef __RVEX__
#include <input.h>
#else
#include <linux/input.h>
#endif

#undef KEY_RIGHTARROW
#undef KEY_LEFTARROW
#undef KEY_UPARROW
#undef KEY_DOWNARROW
#undef KEY_ESCAPE
#undef KEY_ENTER
#undef KEY_TAB
#undef KEY_F1
#undef KEY_F2
#undef KEY_F3
#undef KEY_F4
#undef KEY_F5
#undef KEY_F6
#undef KEY_F7
#undef KEY_F8
#undef KEY_F9
#undef KEY_F10
#undef KEY_F11
#undef KEY_F12
#undef KEY_BACKSPACE
#undef KEY_PAUSE
#undef KEY_EQUALS
#undef KEY_MINUS
#undef KEY_RSHIFT
#undef KEY_RCTRL
#undef KEY_RALT
#undef KEY_LSHIFT
#undef KEY_LCTRL
#undef KEY_LALT

#include "doomdef.h"
#include "i_system.h"
#include "d_event.h"

/**
 * Keyboard input file descriptor.
 */
static int kb_fd = -1;

/**
 * Whether the shift keys are currently down. Bit 0 is left shift, bit 1 is
 * right shift.
 */
static int kb_shift = 0;

/**
 * Scan code to DOOM key code table. The first entry for each code is without
 * shift, the second is with shift.
 */
#define NUM_SCANS 128
static const unsigned char kb_map[2*NUM_SCANS] = {
    /* 00 = unused */       0,              0,
    /* 01 = ESC */          KEY_ESCAPE,     KEY_ESCAPE,
    /* 02 = 1 */            '1',            '!',
    /* 03 = 2 */            '2',            '@',
    /* 04 = 3 */            '3',            '#',
    /* 05 = 4 */            '4',            '$',
    /* 06 = 5 */            '5',            '%',
    /* 07 = 6 */            '6',            '^',
    /* 08 = 7 */            '7',            '&',
    /* 09 = 8 */            '8',            '*',
    /* 0A = 9 */            '9',            '(',
    /* 0B = 0 */            '0',            ')',
    /* 0C = - _ */          '-',            '_',
    /* 0D = = + */          '=',            '=',
    /* 0E = BKSP */         KEY_BACKSPACE,  KEY_BACKSPACE,
    /* 0F = Tab */          KEY_TAB,        KEY_TAB,
    /* 10 = Q */            'q',            'Q',
    /* 11 = W */            'w',            'W',
    /* 12 = E */            'e',            'E',
    /* 13 = R */            'r',            'R',
    /* 14 = T */            't',            'T',
    /* 15 = Y */            'y',            'Y',
    /* 16 = U */            'u',            'U',
    /* 17 = I */            'i',            'I',
    /* 18 = O */            'o',            'O',
    /* 19 = P */            'p',            'P',
    /* 1A = [ { */          '[',            '{',
    /* 1B = ] } */          ']',            '}',
    /* 1C = Enter */        KEY_ENTER,      KEY_ENTER,
    /* 1D = L Ctrl */       KEY_LCTRL,      KEY_LCTRL,
    /* 1E = A */            'a',            'A',
    /* 1F = S */            's',            'S',
    /* 20 = D */            'd',            'D',
    /* 21 = F */            'f',            'F',
    /* 22 = G */            'g',            'G',
    /* 23 = H */            'h',            'H',
    /* 24 = J */            'j',            'J',
    /* 25 = K */            'k',            'K',
    /* 26 = L */            'l',            'L',
    /* 27 = ; : */          ';',            ':',
    /* 28 = ' " */          '\'',           '"',
    /* 29 = ` ~ */          '`',            '~',
    /* 2A = L SH */         KEY_LSHIFT,     KEY_LSHIFT,
    /* 2B = \ | */          '\\',           '|',
    /* 2C = Z */            'z',            'Z',
    /* 2D = X */            'x',            'X',
    /* 2E = C */            'c',            'C',
    /* 2F = V */            'v',            'V',
    /* 30 = B */            'b',            'B',
    /* 31 = N */            'n',            'N',
    /* 32 = M */            'm',            'M',
    /* 33 = , < */          ',',            '<',
    /* 34 = . > */          '.',            '>',
    /* 35 = / ? */          '/',            '?',
    /* 36 = R SH */         KEY_RSHIFT,     KEY_RSHIFT,
    /* 37 = PtScr */        0,              0,
    /* 38 = Alt */          KEY_LALT,       KEY_LALT,
    /* 39 = Spc */          ' ',            ' ',
    /* 3A = CpsLk */        0,              0,
    /* 3B = F1 */           KEY_F1,         KEY_F1, 
    /* 3C = F2 */           KEY_F2,         KEY_F2, 
    /* 3D = F3 */           KEY_F3,         KEY_F3, 
    /* 3E = F4 */           KEY_F4,         KEY_F4, 
    /* 3F = F5 */           KEY_F5,         KEY_F5, 
    /* 40 = F6 */           KEY_F6,         KEY_F6, 
    /* 41 = F7 */           KEY_F7,         KEY_F7, 
    /* 42 = F8 */           KEY_F8,         KEY_F8, 
    /* 43 = F9 */           KEY_F9,         KEY_F9, 
    /* 44 = F10 */          KEY_F10,        KEY_F10,
    /* 45 = Num Lk */       0,              0,
    /* 46 = Scrl Lk */      0,              0,
    /* 47 = NP 7 */         0,              0,
    /* 48 = NP 8 */         0,              0,
    /* 49 = NP 9 */         0,              0,
    /* 4A = NP - */         0,              0,
    /* 4B = NP 4 */         0,              0,
    /* 4C = NP 5 */         0,              0,
    /* 4D = NP 6 */         0,              0,
    /* 4E = NP + */         0,              0,
    /* 4F = NP 1 */         0,              0,
    /* 50 = NP 2 */         0,              0,
    /* 51 = NP 3 */         0,              0,
    /* 52 = NP Ins */       0,              0,
    /* 53 = NP Del */       0,              0,
    /* 54 = */              0,              0,
    /* 55 = */              0,              0,
    /* 56 = */              0,              0,
    /* 57 = F11 */          0,              0,
    /* 58 = F12 */          0,              0,
    /* 59 = */              0,              0,
    /* 5A = */              0,              0,
    /* 5B = */              0,              0,
    /* 5C = */              0,              0,
    /* 5D = */              0,              0,
    /* 5E = */              0,              0,
    /* 5F = */              0,              0,
    /* 60 = NP Enter */     KEY_ENTER,      KEY_ENTER,
    /* 61 = R Ctrl */       KEY_RCTRL,      KEY_RCTRL,
    /* 62 = */              0,              0,
    /* 63 = PrtSc */        0,              0,
    /* 64 = R Alt */        KEY_RALT,       KEY_RALT,
    /* 65 = */              0,              0,
    /* 66 = Home */         0,              0,
    /* 67 = Up */           KEY_UPARROW,    KEY_UPARROW,
    /* 68 = PgUp */         0,              0,
    /* 69 = Left */         KEY_LEFTARROW,  KEY_LEFTARROW,
    /* 6A = Right */        KEY_RIGHTARROW, KEY_RIGHTARROW,
    /* 6B = */              0,              0,
    /* 6C = Down */         KEY_DOWNARROW,  KEY_DOWNARROW,
    /* 6D = PgDn */         0,              0,
    /* 6E = Insert */       0,              0,
    /* 6F = Delete */       0,              0,
    /* 70 = */              0,              0,
    /* 71 = */              0,              0,
    /* 72 = */              0,              0,
    /* 73 = */              0,              0,
    /* 74 = */              0,              0,
    /* 75 = */              0,              0,
    /* 76 = */              0,              0,
    /* 77 = */              0,              0,
    /* 78 = */              0,              0,
    /* 79 = */              0,              0,
    /* 7A = */              0,              0,
    /* 7B = */              0,              0,
    /* 7C = */              0,              0,
    /* 7D = L Winkey */     0,              0,
    /* 7E = R Winkey */     0,              0,
    /* 7F = R Menu */       0,              0
};

void I_Init(void) {
    
    // Initialize sound and graphics subsystems.
    I_InitSound();
    I_InitGraphics();
    
    // Open the keyboard input file.
    kb_fd = open(
#ifdef __RVEX__
        "/dev/input/event1",
#else
        "/dev/input/event2",
#endif
        O_RDONLY | O_NONBLOCK, 0);
    if (kb_fd < 0) {
        perror("open /dev/event");
        exit(1);
    }
    
}

byte* I_ZoneBase(int *size) {
    *size = 6*1024*1024;
    return (byte*)malloc(*size);
}

int I_GetTime(void) {
    struct timeval tp;
    struct timezone tzp;
    int newtics;
    static int basetime = 0;
  
    gettimeofday(&tp, &tzp);
    if (!basetime) {
        basetime = tp.tv_sec;
    }
    newtics = (tp.tv_sec-basetime)*TICRATE + tp.tv_usec*TICRATE/1000000;
    return newtics;
}

void I_StartFrame(void) {
}

void I_StartTic(void) {
    
    // Read keyboard events.
    while (1) {
        
        // Get the next event.
        struct input_event ev;
        if (read(kb_fd, &ev, sizeof(ev)) != sizeof(ev)) {
            break;
        }
        
        // Filter for key press and release info.
        if (ev.type != EV_KEY || ev.value > 1 || ev.code >= NUM_SCANS) {
            continue;
        }
        
        // Convert to DOOM event.
        event_t dev;
        dev.type = ev.value ? ev_keydown : ev_keyup;
        dev.data1 = kb_map[(ev.code << 1) + !!kb_shift];
        dev.data2 = 0;
        dev.data3 = 0;
        
        // Don't post if the scan code has no mapping for DOOM.
        if (!dev.data1) {
            continue;
        }
        
        // Post the event.
        D_PostEvent(&dev);
        
        // Handle shift buttons.
        if (dev.data1 == KEY_LSHIFT) {
            if (ev.value) {
                kb_shift |= 1;
            } else {
                kb_shift &= ~1;
            }
        } else if (dev.data1 == KEY_RSHIFT) {
            if (ev.value) {
                kb_shift |= 2;
            } else {
                kb_shift &= ~2;
            }
        }
        
    }
    
}

ticcmd_t *I_BaseTiccmd(void) {
    static ticcmd_t emptycmd;
    return &emptycmd;
}

void I_Quit(void)
{
    D_QuitNetGame();
    I_ShutdownSound();
    I_ShutdownMusic();
    M_SaveDefaults();
    I_ShutdownGraphics();
    exit(0);
}

byte *I_AllocLow(int length) {
    return (byte*)calloc(1, length);
}

void I_Tactile(int on, int off, int total) {
}

extern boolean demorecording;
void I_Error (char *error, ...) {
    va_list argptr;

    // Message first.
    va_start(argptr,error);
    fprintf(stderr, "Error: ");
    vfprintf(stderr,error,argptr);
    fprintf(stderr, "\n");
    va_end(argptr);

    fflush(stderr);

    // Shutdown. Here might be other errors.
    if (demorecording) {
        G_CheckDemoStatus();
    }

    D_QuitNetGame ();
    I_ShutdownGraphics();
    
    exit(-1);
}
