
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#include "doomstat.h"
#include "i_video.h"
#include "v_video.h"
#include "doomdef.h"

/**
 * Framebuffer file descriptor.
 */
static int fb_fd = -1;

#ifdef __RVEX__
#include <fb.h>
#else
#include <linux/fb.h>
#endif

/**
 * Framebuffer info.
 */
struct fb_var_screeninfo fb_info;

/**
 * Currently selected palette if we're in emulation mode with a 32bpp
 * framebuffer.
 */
typedef struct color_t {
    unsigned char r;
    unsigned char g;
    unsigned char b;
    unsigned char a;
} color_t;
static color_t current_palette[256];

void I_InitGraphics(void) {
    
    // Open the framebuffer device.
    fb_fd = open("/dev/fb0", O_RDWR, 0);
    if (fb_fd < 0) {
        perror("open /dev/fb0");
        exit(1);
    }
    
    // Query framebuffer information.
    int res = ioctl(fb_fd, FBIOGET_VSCREENINFO, &fb_info);
    if (res < 0) {
        perror("ioctl FBIOGET_VSCREENINFO /dev/fb0");
        exit(1);
    }
    
#ifdef __RVEX__
    
    // If we're running on the rvex, request 320x240 8bpp. If we're on Linux,
    // well... let's not try.
    fb_info.xres = 320;
    fb_info.yres = 240;
    fb_info.bits_per_pixel = 8;
    res = ioctl(fb_fd, FBIOPUT_VSCREENINFO, &fb_info);
    if (res < 0) {
        perror("ioctl FBIOPUT_VSCREENINFO /dev/fb0");
        exit(1);
    }
    
#endif
    
    printf("FB: %uX%u %ubpp\n", fb_info.xres, fb_info.yres, fb_info.bits_per_pixel);
    
    if ((fb_info.bits_per_pixel != 8) && (fb_info.bits_per_pixel != 32)) {
        fprintf(stderr, "Must run in 8bpp or 32bpp!\n");
        exit(1);
    }
    
}

void I_ShutdownGraphics(void) {
    
    // Close the framebuffer device.
    if (fb_fd >= 0) {
        close(fb_fd);
        fb_fd = -1;
    }
    
}

void I_SetPalette(byte *palette) {
    
    if (fb_info.bits_per_pixel == 8) {
        
        // Update palette.
        
        
    } else {
        
        // Save palette for emulation.
        int i;
        for (i = 0; i < 256; i++) {
            current_palette[i].a = 0;
            current_palette[i].b = *palette++;
            current_palette[i].g = *palette++;
            current_palette[i].r = *palette++;
        }
        
    }
    
}

void I_UpdateNoBlit(void) {
}

void I_FinishUpdate(void) {
    
    if (fb_info.bits_per_pixel == 8) {
        
        if (fb_info.xres = 320) {
            
            // Write directly into the framebuffer.
            lseek(fb_fd, 0, SEEK_SET);
            write(fb_fd, screens[0], 320*240);
            
        } else {
            
            // Write line-by-line with the correct stride.
            int y;
            for (y = 0; y < 240; y++) {
                lseek(fb_fd, y * fb_info.xres, SEEK_SET);
                write(fb_fd, screens[0] + y * 320, 320);
            }
            
        }
        
    } else {
        
        // Write with palette emulation.
        int x, y;
        byte *doom_pix = screens[0];
        static color_t linebuf[320];
        for (y = 0; y < 240; y++) {
            for (x = 0; x < 320; x++) {
                linebuf[x] = current_palette[(unsigned char)(*doom_pix++)];
            }
            lseek(fb_fd, y * fb_info.xres * 4, SEEK_SET);
            write(fb_fd, linebuf, 320 * 4);
        }
        
    }
    
}

void I_WaitVBL(int count) {
}

void I_ReadScreen(byte* scr) {
    memcpy(scr, screens[0], SCREENWIDTH*SCREENHEIGHT);
}

void I_BeginRead(void) {
}

void I_EndRead(void) {
}
