
r-VEX DOOM
==========

This is a port of the shareware DOOM sources for the r-VEX ML605 demo platform.
Its purpose is to test the newlib+libfs syscalls thoroughly.

It currently doesn't work yet. RIP

Linux
-----

You can also build a semi-working Linux version with this source tree. It uses
the same device files that the r-VEX does (`fb0`, `dsp`, `input/event`) and
thus may or may not work on present-day systems. For me it requires padsp to
emulate `/dev/dsp`, and it only shows up in a rescue terminal because the
desktop is hardware accelerated and overrules `/dev/fb0` nowadays.

To get music to work (on Linux anyway; the r-VEX is way too slow) you need to
download a soundfont, name it data/soundfont.sf2, and remove the
`DISABLE_MUSIC` define in `i_sound.c`.

